#include "Image.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void ecrirePnm(const Image & img, const string & nomFichier){
    ofstream fichier(nomFichier);
    fichier << "P2"<<endl;
    fichier << img.getLargeur()<<" "<<img.getHauteur()<<endl;
    fichier << "255"<<endl;
    for(int i = 0 ; i< img.getLargeur(); i++){
        for(int j=0; j< img.getHauteur(); j++){
            fichier << img.pixel(i,j) << endl;
        }
    }
}

void remplir(Image &img){
    for(int i = 0 ; i< img.getLargeur(); i++){
        for(int j=0; j< img.getHauteur(); j++){
            img.pixel(i,j) = 0;
        }
    }
}


int main() {
    Image img(50,50);
    remplir(img);
    ecrirePnm(img,"img.pnm");
    return 0;
}

