#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) {};

TEST(GroupImage, Image_test1)  {
    Image i(5,3);
    CHECK_EQUAL(i.getLargeur(),5);
    CHECK_EQUAL(i.getHauteur(),3);
}

TEST(GroupImage, Image_test2)  {
    Image i(5,3);
    i.setPixel(1,1,0);
    CHECK_EQUAL(i.getPixel(1,1),0);
}

TEST(GroupImage, Image_test3)  {
    Image i(5,3);
    i.pixel(1,1) = 255;
    int pix = i.pixel(1,1);
    CHECK_EQUAL(pix,255);
}


