#include <cctype>
#include <iostream>
#include "fibo.hpp"
using namespace std;

int fibonacciIteratif(int n){
	
    int a = 1, b = 1;
    for (int i = 3; i <= n; i++) {
        int c = a + b;
        a = b;
        b = c;
    }           
    return b;
}

int fibonacciRecursif(int x) {
    if (x == 1) {
        return 1;
    } 
    if (x==0){
		return 0;
	}
    return fibonacciRecursif(x-1)+fibonacciRecursif(x-2);
}

