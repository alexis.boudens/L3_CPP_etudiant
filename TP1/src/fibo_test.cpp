#include "fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo){};
TEST(GroupFibo, test_fibo_1){
    int result = fibonacciIteratif(7);
    CHECK_EQUAL(13,result);
}

TEST(GroupFibo, test_fibo_2){
    int result = fibonacciRecursif(7);
    CHECK_EQUAL(13, result);
}
