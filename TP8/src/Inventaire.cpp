#include "Inventaire.hpp"


std::ostream & operator<<(std::ostream & os, const Inventaire & i){
    for(const Bouteille & bout : i._bouteilles){
        os << bout;
    }
    return os;
}
