#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

    //_inventaire._bouteilles.push_back(Bouteille{"Zed","1609-12-4",7.2});

    chargerInventaire();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::stringstream Controleur::getTexte() const{
    std::stringstream ss;
    ss << _inventaire;
    return ss;
}

void Controleur::chargerInventaire(){
    _inventaire._bouteilles.push_back(Bouteille{"boisson","1989-02-04",7.2});
    for (auto & v : _vues)
      v->actualiser();
}


