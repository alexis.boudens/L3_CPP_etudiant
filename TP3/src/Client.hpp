#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <iostream>
#include <string>

class Client {
    private:
        int _id;
        std::string _nom;

    public:
        Client(int id, const std::string & nom);
        void afficherClient() const;
        int getId() const;
        const std::string & getNom() const;
};

#endif // CLIENT_HPP
