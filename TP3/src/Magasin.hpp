#ifndef MAGASIN_HPP
#define MAGASIN_HPP

#include <iostream>
#include <string>
#include <vector>
#include "Produit.hpp"
#include "Location.hpp"
#include "Client.hpp"

class Magasin {
    private:
        std::vector<Client> _clients;
        std::vector<Produit> _produits;
        std::vector<Location> _locations;
        int _idCourantClient;
        int _idCourantProduit;

    public:
        Magasin();
        int nbClients()const;
        void ajouterClient(const std::string & nom);
        void afficherClient() const;
        void supprimerClient(int idClient);

        int nbProduits()const;
        void ajouterProduit(const std::string & nom);
        void afficherProduits() const;
        void supprimerProduit(int idProduit);

        int nbLocations() const;
        void ajouterLocation(int idClient, int idProduit);
        void afficherLocations()const;
        bool trouverClientDansLocation(int idClient) const;
        bool trouverProduitDansLocation(int idProduit)const;
};

#endif // MAGASIN_HPP
