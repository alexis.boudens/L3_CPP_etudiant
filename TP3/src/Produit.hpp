#ifndef PRODUIT_HPP
#define PRODUIT_HPP

#include <iostream>
#include <string>

class Produit {
    private:
        int _id;
        std::string _description;

    public:
        Produit(int id, const std::string & description);
        void afficherProduit() const;
        int getId() const;
        const std::string & getDescription() const;
};

#endif // PRODUIT_HPP
