#include "Produit.hpp"

Produit::Produit(int id, const std::string & desc):
    _id(id), _description(desc)
{}

void Produit::afficherProduit() const {
    std::cout<<"Produit ("<<_id<<","<<_description<<")"<<std::endl;
}


int Produit::getId() const {
    return _id;
}

const std::string & Produit::getDescription() const {
    return _description;
}
