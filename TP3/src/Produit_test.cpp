#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_test1)  {
    Produit prod(1,"test");
    CHECK_EQUAL(prod.getId(), 1);
    CHECK_EQUAL(prod.getDescription(), "test");
}



