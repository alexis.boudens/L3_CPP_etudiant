#include "Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_test1)  {
    Magasin mag;
    CHECK_EQUAL(mag.nbClients(), 0);
    mag.ajouterClient("test");
    CHECK_EQUAL(mag.nbClients(), 1);
    mag.supprimerClient(0);
    CHECK_EQUAL(mag.nbClients(), 0);
}

TEST(GroupMagasin, Magasin_test2)  {
    Magasin mag;
    CHECK_EQUAL(mag.nbProduits(), 0);
    mag.ajouterProduit("test");
    CHECK_EQUAL(mag.nbProduits(), 1);
    mag.supprimerProduit(0);
    CHECK_EQUAL(mag.nbProduits(), 0);
}

/*
TEST(GroupMagasin, Magasin_test3){
    Magasin mag;
    mag.ajouterLocation(0,0);
    CHECK_EQUAL(mag.nbLocations(),1);
}
*/
