#include "Doubler.hpp"
#include "Location.hpp"
#include "Client.hpp"
#include "Magasin.hpp"
#include "Produit.hpp"

#include <iostream>

int main() {
    //std::cout << doubler(21) << std::endl;
    //std::cout << doubler(21.f) << std::endl;

    Magasin mag;
    mag.ajouterClient("toto");

    try {
        mag.supprimerClient(6);
    }
    catch (std::string v){
        std::cerr << "Ce client n'existe pas ! " <<v << std::endl;
    }

    mag.afficherClient();

    mag.ajouterProduit("produit");

    try {
        mag.supprimerProduit(1);
    }
    catch (std::string v){
        std::cerr <<"Ce produit n'existe pas ! " <<v<<std::endl;
    }

    return 0;
}

