#include "Magasin.hpp"


Magasin::Magasin(){
    _idCourantClient =0;
    _idCourantProduit =0;
}

int Magasin::nbClients()const{
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom){
    Client newCli(_idCourantClient, nom);
    _clients.push_back(newCli);
    _idCourantClient++;
}

void Magasin::afficherClient()const {
    for( auto const &i: _clients){
        std::cout<<i.getId()<<"/"<<i.getNom()<<std::endl;
    }
}

void Magasin::supprimerClient(int idClient){
    if(_clients[idClient].getNom() == ""){
        throw std::string("ERROR CLIENTS");
    }
    std::swap(_clients[idClient], _clients[_idCourantClient-1]);
    _clients.pop_back();
}

int Magasin::nbProduits()const{
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string &nom){
    Produit newProd(_idCourantProduit, nom);
    _produits.push_back(newProd);
    _idCourantProduit++;
}

void Magasin::afficherProduits()const {
    for( auto const &i: _produits){
        std::cout<<i.getId()<<"/"<<i.getDescription()<<std::endl;
    }
}

void Magasin::supprimerProduit(int idProduit){
    if(_produits[idProduit].getDescription() == ""){
        throw std::string("ERROR PRODUITS");
    }
    std::swap(_produits[idProduit], _produits[_idCourantProduit-1]);
    _produits.pop_back();
}

int Magasin::nbLocations()const {
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    Location * l = new Location(idClient,idProduit);
    _locations.push_back(*l);
}

void Magasin::afficherLocations()const{
    for(auto const &i: _locations){
        i.afficherLocation();
    }
}

bool Magasin::trouverClientDansLocation(int idClient) const{
    for(auto i : _locations){
        if(i._idClient == idClient){
            return true;
        }
    }
    return false;
}

bool Magasin:: trouverProduitDansLocation(int idProduit) const{
    for(auto i: _locations){
        if(i._idProduit == idProduit){
            return true;
        }
    }
    return false;
}
