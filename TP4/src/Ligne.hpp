#ifndef LIGNE_HPP
#define LIGNE_HPP
#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"
#include <iostream>

class Ligne : public FigureGeometrique {
    private:
        Point _p0;
        Point _p1;
    public:
        Ligne(Couleur const & couleur, Point const & p0, Point const & p1);
        void afficher() const;
        const Point & getP0()const;
        const Point & getP1()const;

};

#endif // LIGNE_HPP
