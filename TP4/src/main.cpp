#include "Ligne.hpp"

#include <iostream>
#include <vector>

int main() {

    Couleur c;
    c._b = c._g = c._r = 0;

    Couleur c2;
    c._r = 0.5;
    c._g = 0.1;
    c._b = 0.9;

    Point p1;
    p1._x = 100;
    p1._y = 200;

    Point p2;
    p2._x = 150;
    p2._y = 250;

    std::vector<FigureGeometrique*> figures {new Ligne(c,p1,p2), new Ligne(c2,p1,p2)};

    for(FigureGeometrique * fig : figures){
        Couleur tmp = fig->getCouleur();
        std::cout<<tmp._r<<"/"<<tmp._g<<"/"<<tmp._b<<std::endl;
    }

    for(FigureGeometrique * fig : figures){
        delete fig;
    }

    return 0;
}

