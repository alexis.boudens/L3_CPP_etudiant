#include "Livre.hpp"

Livre::Livre(const std::string &titre, const std::string &auteur, int annee):
    _titre(titre), _auteur(auteur), _annee(annee)
{
    if(titre.find(";") != std::string::npos){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    if(auteur.find(";") != std::string::npos){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    if(titre.find("\n") != std::string::npos){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }
    if(auteur.find("\n") != std::string::npos){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }
}

Livre::Livre(){
    _annee = 0;
}

const std::string  Livre::getTitre() const{
    return _titre;
}

const std::string  Livre::getAuteur() const{
    return _auteur;
}

int Livre::getAnnee() const{
    return _annee;
}

std::ostream & operator<<(std::ostream &os, const Livre &l){
    os <<l.getTitre()<<";"<<l.getAuteur()<<";"<<l.getAnnee();
    return os;
}

bool Livre::operator <(const Livre &l){
    if(_auteur < l.getAuteur()){return true;}
    if(_auteur == l.getAuteur()){
        if(_titre < l.getTitre()){
            return true;
        }
    }
    return false;
}

bool Livre::operator ==(const Livre &l){
    return (_auteur == l.getAuteur() && _titre == l.getTitre() && _annee == l.getAnnee());
}















