#include <string>
#include <iostream>
class Livre{
    private:
        std::string _titre;
        std::string _auteur;
        int _annee;

    public:
        Livre();
        Livre(std::string const & titre, std::string const & auteur, int  annee);
        std::string  const getTitre() const;
        std::string  const getAuteur() const;
        int getAnnee() const;
        bool operator < (const Livre &l);
        bool operator == (const Livre &l);

};

std::ostream & operator<<(std::ostream &os, const Livre &l);


