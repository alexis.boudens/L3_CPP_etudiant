#include "Doubler.hpp"
#include "liste.hpp"

#include <iostream>

int main() {
    std::cout <<"Double de "<<21<<" : "<< doubler(21) << std::endl;

    //Pointeur
    int a = 42;
    int *p = &a;
    *p = 37;
    std::cout<<"Exo pointeur : "<<a<<std::endl;

    //Allocation dynamique
    int *t ;
    t = new int[10];
    delete [] t;
    t = nullptr;

    //Fuites mémoires

    Liste l;
    l.ajouterDevant(13);
    l.ajouterDevant(37);

    for(int i=0;i<l.getTaille();i++){
        std::cout<<l.getElement(i);
    }

    return 0;
}

