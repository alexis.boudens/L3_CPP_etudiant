#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_test1)  {
    Liste l;
    CHECK(l._tete == nullptr);

    l.ajouterDevant(4);
    CHECK(l._tete->_valeur == 4);

    CHECK_EQUAL(l.getTaille(),1);

    CHECK_EQUAL(l.getElement(0),4);
}

TEST(GroupListe, Liste_test2){
    Liste l;
    l.ajouterDevant(4);
    CHECK_EQUAL(l.getTaille(),1);
}

TEST(GroupListe, Liste_test3){
    Liste l;
    l.ajouterDevant(1);
    l.ajouterDevant(2);
    l.ajouterDevant(3);
    CHECK_EQUAL(l.getElement(2),1);
}







