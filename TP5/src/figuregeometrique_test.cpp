#include "figuregeometrique.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFig) { };

TEST(GroupFig, Fig_test1)  {
    Couleur c{1,2,3};
    FigureGeometrique f (c);

    Couleur c1=f.getCouleur();
    CHECK_EQUAL(c._r,c1._r);
    CHECK_EQUAL(c._g,c1._g);
    CHECK_EQUAL(c._b,c1._b);
}
