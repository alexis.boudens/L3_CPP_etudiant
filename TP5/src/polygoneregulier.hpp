#ifndef POLYGONEREGULIER_HPP
#define POLYGONEREGULIER_HPP
#include "figuregeometrique.hpp"
#include "point.hpp"

class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbPoints;
    Point* _points;
public:
    PolygoneRegulier(const Couleur & couleur,const Point & centre,int rayon,int nbCotes);
    void afficher() const;
};

#endif // POLYGONEREGULIER_HPP
