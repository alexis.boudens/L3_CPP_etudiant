#include "ZoneDessin.hpp"
#include "point.hpp"
#include "couleur.hpp"
#include "ligne.hpp"

ZoneDessin::ZoneDessin(){
    Point p0{0,0}, p1{1,1};
    Couleur cou{1,1,1};
    Ligne l(cou,p0,p1);
    _figures.push_back(&l);
}

ZoneDessin::~ZoneDessin(){
    for(auto x: _figures){
        delete x;
    }
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context){
    auto window = get_window();
    return true;
}
