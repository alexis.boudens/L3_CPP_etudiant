#include<iostream>
#include"couleur.hpp"
#include"figuregeometrique.hpp"
#include"ligne.hpp"
#include"point.hpp"
#include"polygoneregulier.hpp"
#include "ViewerFigures.hpp"
#include <gtkmm.h>
#include "ZoneDessin.hpp"

int main(int argc,char ** argv){

    ViewerFigures vf(argc,argv);
    vf.run();

    ZoneDessin zd;

    return 0;
}
