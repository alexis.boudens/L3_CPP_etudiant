#include "FigureGeometrique.hpp"
#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFigure) { };

TEST(GroupFigure, Figure_test1)  {
    Couleur c;
    c._r = c._g = c._b = 1.0;
    FigureGeometrique f(c);
    Couleur nouv = f.getCouleur();
    CHECK_EQUAL(nouv._b,1.0);
    CHECK_EQUAL(nouv._r,1.0);
    CHECK_EQUAL(nouv._g,1.0);
}

TEST(GroupFigure, Figure_test2)  {
    Couleur c;
    c._r = c._g = c._b = 1.0;
    Point p1;
    Point p2;
    p1._x = p1._y = 0;
    p2._x = 100;
    p2._y = 200;
    Ligne f(c,p1,p2);
    Point nouvP0 = f.getP0();
    Point nouvP1 = f.getP1();
    CHECK_EQUAL(p1._x,nouvP0._x);
    CHECK_EQUAL(p2._y,nouvP1._y);
}


