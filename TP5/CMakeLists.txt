cmake_minimum_required( VERSION 3.0 )
project( TP4 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG REQUIRED cpputest )
pkg_check_modules( GTK REQUIRED gtkmm-3.0 )
include_directories( ${PKG_INCLUDE_DIRS} )
include_directories( ${GTK_INCLUDE_DIRS} )

# programme principal
add_executable( maintp4.out
    src/maintp4.cpp
    src/figuregeometrique.cpp
    src/ligne.cpp
    src/polygoneregulier.cpp
    src/ViewerFigures.cpp
    src/ZoneDessin.cpp)
target_link_libraries( maintp4.out ${GTK_LIBRARIES} )

# programme de test
add_executable( maintp4_test.out
    src/maintp4_test.cpp

    src/figuregeometrique.cpp
    src/figuregeometrique_test.cpp

    src/ligne.cpp
    src/ligne_test.cpp )
target_link_libraries( maintp4_test.out ${PKG_LIBRARIES} )
